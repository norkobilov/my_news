from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser


# Birinchi forma ro'yxatdan o'tish uchun
class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'age',)


# Ikkinchi forma o'zgartirish uchun
class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'age',)
