from django.urls import path
from .views import SignUpView, RegisterPage

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('register/', RegisterPage.as_view(), name='register'),
]
