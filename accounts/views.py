from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView
from .forms import CustomUserCreationForm
from . import models, permissions


# Create your views here.
class SignUpView(CreateView):
    # permisson_class = (permissions.IsTeacherUser)
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


class RegisterPage(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'register.html')

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        age = request.POST['age']
        password = request.POST['password']
        isuser = models.CustomUser.objects.filter(username=username)
        user = models.CustomUser.objects.create_user(username=username, first_name=first_name, last_name=last_name,
                                                     email=email, age=age, password=password)
        print(user)
        return render(request, 'home.html')
