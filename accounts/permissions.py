from rest_framework import permissions

class IsTeacherUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_teacher and request.user.status == 'accept'

    def has_object_permission(self, request, view, obj):
        return request.user and request.user.is_teacher and request.user.status == 'accept'