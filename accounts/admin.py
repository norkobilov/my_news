from django.contrib import admin

# Biz yaratgan formalar va userlarni bog'lash uchun ularni admin.py ga ko'rsatib qo'yish kerak
from django.contrib.auth.admin import UserAdmin  # UserAdmin djangoni o'zining kutubxonasi
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email','username', 'first_name', 'last_name', 'age', 'is_staff']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('age',)}),
    )
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('age',)}),
    )


# Register your models here.
admin.site.register(CustomUser, CustomUserAdmin)
