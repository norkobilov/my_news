# from rest_framework import views, status, mixins
# from rest_framework.response import Response
# from .. import models
# from . import serializers
#
#
# class ArticleView(views.APIView):
#     def get(self, request, pk=0):
#         if(pk==0):
#             qs = models.Article.objects.all()
#             sd = serializers.ArticleSerializer(qs, many=True).data
#             return Response(sd, status.HTTP_200_OK)
#         else:
#             qs = models.Article.objects.filter(id=pk).first()
#             sd = serializers.ArticleSerializer(qs, many=False).data
#             return Response(sd, status.HTTP_200_OK)
#
#
#
#     def post(self, request):
#         sd = serializers.ArticleSerializer(data=request.data)
#         if (sd.is_valid()):
#             sd.save()
#             return Response(sd.data, status=status.HTTP_201_CREATED)
#         return Response(sd.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def put(self, request, pk, format=None):
#         snippet = models.Article.objects.filter(id=pk).first()
#         serializer = serializers.ArticleSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         snippet = models.Article.objects.filter(id=pk).first()
#         snippet.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
